
const fontSizeKey = 'font-size'
const bgColor = 'bg-color'

const setFontSize = (fontSize) => { console.log('setFontSize');

    localStorage[fontSizeKey] = JSON.stringify(fontSize)
    applyFontSize()
}

const getFontSize = () => { console.log('getFontSize');
   if (!localStorage[fontSizeKey])
        setFontSize(1.5)

    return JSON.parse(localStorage[fontSizeKey]) 
}

const changeFontSize = (n) => { console.log('changeFontSize');
    let fSize = getFontSize() || 2

    fSize+= n

    // limits the size of font
    if (fSize > 3) {
        fSize = 3
    } else if (fSize < 1) {
        fSize = 1
    }

    setFontSize(fSize)
    applyFontSize()
}

const applyFontSize = () => {
    console.log('Apply font size');
    
    if (getFontSize) {
        $('.end-game-text').css('font-size', getFontSize() + 'em')
    }
    else {
        $('.end-game-text').css('font-size', '2em')
        console.log('TA NO ELSE');
        
    }
}

const setBgColor = (color) => {
    localStorage[bgColor] = JSON.stringify(color)
    applyBgColor()
}

const getBgColor = () => {
    if (!localStorage[bgColor])
        setBgColor('rgb(185, 171, 170)')
    
    return JSON.parse(localStorage[bgColor]) 
}

const applyBgColor = () => { console.log('Apply bg color');
    if (getBgColor()) {
        $('body').css('background-color', getBgColor())

    }  
}

 
