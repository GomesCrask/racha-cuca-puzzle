function startPuzzle(x, level) {
    let actionButtons = '' 

    // only show action buttons for next and repeat if current level is not the last
    if (currentStage !== levels.length -1) {
        console.log('IS NOT LAST LEVEL');
        
        actionButtons = `
        <br><br>
        <a class="pure-button button-lg restart-puzzle end-game-button" onClick="changeStage()" >Próxima fase</a>

        
        `
    } else {
        console.log('IS LAST LEVEL');
        actionButtons = `
        
        
        <hr>
        <a class="pure-button button-lg restart-puzzle" href="final.html">Finalizar</a>
        
        
        `
    }
    

    $('#puzzle-containment').html('')
    $('#puzzle-containment').html(` <div class="pure-g " style="max-width:1280px;margin:auto; padding:15px" >
            <div class="pure-u-1 pure-u-md-1-2"><div style="margin:10px"  >
                <img id="source_image" class="pure-img" src="${level.imagePath}">
            </div></div>
            <div class="pure-u-1 pure-u-md-1-2" >
                <div id="pile" style="margin:10px" >
                    <div id="pannel" class="modal" style="display:none;text-align:center;position:relative;top:25%;width: 100%">
                        
                        ${actionButtons}
                        <br><br> 


                        <div class="modal-background">
                        </div>
                    </div>
                    <div id="puzzle_solved" class="modal" style="display:none;text-align:center;top:25%">
                        <img id="avatar" src="background/avatar.png">
                        <h2 style="margin:0 0 20px" class="end-game-text modal-content">
                            ${level.text}
                        </h2>
                        <br>
                        <audio controls> 
                            <source src="${level.audioPath}" type="audio/mpeg">
                            Your browser does not support the audio element.
                        </audio>
                        <hr>
                        <a class="pure-button button-lg " onClick="closeModal()" >Fechar modal</a>
                        <a class="pure-button button-lg restart-puzzle end-game-button" onClick="changeStage()" >Próxima fase</a>
                        <br><br> 
                    </div>
                </div>
            </div>
            <div id="action-menu" >
            <div>
                <a class=" trails-list-item waves-effect waves-light btn action-button" href="/">Menu Principal</a>
                <label class=" trails-list-item waves-effect waves-light btn action-button" href="#" onclick="restartPiece()">Reiniciar Quebra Cabeça</label>
                <label class=" trails-list-item waves-effect waves-light btn action-button" href="#" onclick="changeStage()">Pular</label>
            </div>
        </div>
        </div>`)

    $('#puzzle_solved').hide();
    $('#source_image').snapPuzzle({
        rows: x, columns: x,
        pile: '#pile',
        containment: '#puzzle-containment',
        onComplete: function () {
            $('#source_image').fadeOut(150).fadeIn();

            setTimeout(() => {
                $('#puzzle_solved').show();
                $('#pannel').show();
                
                $('.modal').modal();
                applyFontSize()
            }, 1500)
        }
    });
}

function restartPuzzle() {
    console.log('restart');
    
    window.location.reload()
}

 

function changeStage() {
    currentStage++  

    const currentGridSize = 
          parseInt(currentStage * dificultMultiplyer) >= minGridSize 
                ? parseInt(currentStage * dificultMultiplyer) 
                : minGridSize

                
    startPuzzle(currentGridSize , levels[currentStage])
}

function restartPiece() {
    const currentGridSize = 
    parseInt(currentStage * dificultMultiplyer) >= minGridSize 
          ? parseInt(currentStage * dificultMultiplyer) 
          : minGridSize

          
startPuzzle(currentGridSize , levels[currentStage])
}

function Level(imagePath, text, audioPath) {
    return {
        imagePath,
        text,
        audioPath,
    }
}

// ===========


$(function () {
    $('#pile').height($('#source_image').height());

    startPuzzle(2, levels[currentStage]); 

    $('.restart-puzzle').click(function () {
        $('#source_image').snapPuzzle('reset');
        restartPuzzle()
        // startPuzzle($(this).data('grid'));
    });
    $(window).resize(function () {
        $('#pile').height($('#source_image').height());
        $('#source_image').snapPuzzle('refresh');
    });

    // apply font
    applyFontSize()
    applyBgColor()

});


function closeModal() {
    $('.modal-background').hide()
    $('#puzzle_solved').hide()
}