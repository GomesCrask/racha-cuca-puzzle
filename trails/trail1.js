// configurações da aplicação

const dificult = 1
const minGridSize = 2
const dificultMultiplyer = .5
let currentStage = 0
const levels = [
		new Level(
          'images/mg-capela-curialde-de-sao-francisco.jpg',
          'A Igreja São Francisco de Assis da Pampulha, em Belo Horizonte, Minas Gerais, foi inaugurada em 1943. As linhas curvas da igreja seduziram artistas e arquitetos, mas escandalizaram o acanhado ambiente cultural da cidade, de tal forma, que as autoridades eclesiásticas não permitiram, por muitos anos, a consagração da capela devido à sua forma inusitada e ao painel de Portinari onde se vê um cachorro representando um lobo junto à São Francisco de Assis, a igreja permaneceu durante catorze anos proibida ao culto. Aos olhos do arcebispo Dom Antônio dos Santos Cabral a igrejinha era apenas um galpão.',
          'audio/mg-capela-sao-francisco.wav'),
	   new Level(
		  'images/mg-ccbb.jpg',
		  'O Centro Cultural Banco do Brasil (CCBB) é uma rede de espaços culturais geridas e mantidas pelo Banco do Brasil, com o objetivo de disseminar a cultura pela população. Atualmente, encontra-se instalado em quatro capitais brasileiras: Rio de Janeiro, São Paulo, Belo Horizonte e Brasília.',
		  'audio/mg-ccbb.wav'),
	   new Level(
	  'images/mg-memorial-minas-gerais.jpg',
	  'O Museu Memorial Minas Gerais Vale é um museu artístico e cultural dedicado às tradições do povo mineiro localizado em Belo Horizonte, Minas Gerais. Ambientes que misturam o real e o virtual reconstroem o universo de escritores mineiros, o mundo das fazendas, das tribos indígenas e quilombos, do barroco, das festas populares, do artesanato, da política, e da arqueologia do solo mineiro.',
	  'audio/mg-memorial-minas-gerais.wav'),
	   new Level(
	  'images/mg-mm-gerdau.jpg',
	  'O Museu das Minas e do Metal é um museu brasileiro localizado na cidade de Belo Horizonte, Minas Gerais.Faz parte do Circuito Cultural Praça da Liberdade e foi inaugurado em 22 de março de 2010 com projeto arquitetônico de Paulo Mendes da Rocha e museográfico de Marcello Dantas. O espaço tem a proposta de relevar a importância cotidiana e econômica dos minérios e suas implicações culturais e sociais.',
	  'audio/mg-mm-gerdau.wav'),
	   new Level(
	  'images/mg-parque-municipal-americo-renne.jpg',
	  'O Parque Municipal Américo Renné Giannetti, com 180.000 m², de área cercada e com guaritas em todas as entradas, é o principal parque de Belo Horizonte.O projeto inicial foi elaborado pelo arquiteto-jardineiro Paul Villon natural da França e aluno do naturalista também francês Glaziou, responsável pelo Jardim-Parque da Aclamação, no Rio de Janeiro. O parque foi inaugurado junto com a cidade. Porém, com o tempo, o parque perdeu mais que a metade de seu tamanho original.',
	  'audio/mg-parque-municipal-americo-renne.wav'),
]

