// configurações da aplicação

const dificult = 1
const minGridSize = 2
const dificultMultiplyer = 1.6
let currentStage = 0
const levels = [
     // new Level(
     //      'images/gb-egito-nefertirti.jpg',
     //      'Nefertiti foi uma rainha da décima oitava dinastia do Antigo Egito, esposa principal do faraó Aquenáton. Nefertiti e o seu esposo tornaram-se conhecidos pela revolução religiosa, na qual adoravam apenas um deus, Áton, ou o disco solar. Aquenáton e Nefertiti foram responsáveis por criarem uma nova religião que alterou as formas de religião dentro do Egito. Com o seu esposo, reinou naquele que foi o período mais próspero na história do Antigo Egito.',
     //      'audio/gb-egito-nefertiti.wav'),
	// new Level(
     //      'images/gb-grecia-artemis.jpg',
     //      'Ártemis é deusa da lua, da caça, dos animais selvagens, da região selvagem, do parto e da virgindade e protetora das meninas na antiga religião grega. Foi descrita como a melhor caçadora entre deuses e mortais. Arco e flechas são seus companheiros constantes. ',
     //      'audio/gb-grecia-artemis.wav'),
	// new Level(
     //      'images/gb-igreja-de-la-madelaine.jpg',
     //      'A Igreja da Madalena, situada perto da Praça da Concórdia, em Paris, na França, é uma igreja católica consagrada a Santa Maria Madalena. Se destaca pela arquitetura em forma de templo clássico grego. No local, ocorre a tradicional Lavagem da Madeleine na qual brasileiros radicados na capital francesa e franceses comparecem ao desfile de música e folclore, inspirada pela lavagem das escadarias da Igreja do Senhor do Bonfim, na Bahia',
     //      'audio/gb-igreja-de-la-madelaine.wav'),
	new Level(
          'images/gb-node-dame-la-grande-pointiers.jpg',
          'A Igreja Notre-Dame la Grande de Poitiers é uma igreja de estilo românico localizada em Poitiers, na França. Sua fachada esculpida é considerada uma obra-prima unanimemente reconhecida da arte religiosa deste período. O interior do edifício é típico do estilo românico : é uma construção de paredes maciças com pilares que sustentavam arcos redondos. No interior há janelas pequenas, é um interior pouco iluminado e com uma predominância das linhas horizontais.',
          'audio/gb-notre-daime-de-la-pointiers.wav'),
	new Level(
          'images/gb-o-pensador.jpeg',
          'O Pensador é uma das mais famosas esculturas de bronze do escultor francês Auguste Rodin. Retrata um homem em meditação soberba, lutando com uma poderosa força interna. Originalmente chamado de O Poeta, a peça era parte de uma comissão do Museu de Arte Decorativa em Paris para criar um portal monumental baseada na Divina Comédia, de Dante Alighieri.',
          'audio/gb-o-pensador.wav'),
	new Level(
          'images/gb-opera-garnier.jpg',
          'A Ópera Garnier ou Palais Garnier é considerado uma das obras-primas da arquitetura de seu tempo. Construído em estilo neobarroco, é o décimo terceito teatro a hospedar a Ópera de Paris, desde sua fundação por Luís quatorze, em 1669. Sua capacidade é de 1979 espectadores sentados.',
          'audio/gb-opera-garnier.wav'),
	new Level(
          'images/gb-palacio-de-versailles.jpg',
          'Palácio de Versalhes é um castelo real localizado na cidade de Versalhes, uma aldeia rural à época de sua construção, mas atualmente um subúrbio de Paris. Desde 1682, quando Luís quatorze se mudou de Paris, até a família real ser forçada a voltar à capital em 1789, a Corte de Versalhes foi o centro do poder do Antigo Regime na França. ',
          'audio/gb-palacio-de-versailles.wav'),
	// new Level(
     //      'images/gb-petro-i.jpg',
     //      'O Cavaleiro de Bronze é uma estátua equestre de Pedro o Grande em São Petersburgo, na Rússia. Pedro, o Grande, foi importante na modernização e ocidentalização da Rússia, país que já estava muito desfasado em relação às potências ocidentais. Também deu ao seu país grande poder depois de derrotar a Suécia na Grande Guerra do Norte, que ficou marcada pela sua grande vitória na Batalha de Poltava em 1709. Ao se aperceber de que a Rússia era socialmente e tecnicamente atrasada, resolveu abrir uma janela para o Ocidente, já como czar, a fim de ingressar no país ideias europeias de progresso.',
     //      'audio/gb-petro-i.wav'),
	new Level(
          'images/gb-pieta-michellangello.jpg',
          'A Pietà (em português Piedade) de Michelangelo é talvez a pietà mais conhecida e uma das mais famosas esculturas feitas pelo artista. Representa Jesus morto nos braços de sua mãe.',
          'audio/gb-pita-michellangelo.wav'),
	new Level(
          'images/gb-sanssouci.jpg',
          'Sanssouci é o antigo palácio de Verão de Frederico o Grande, Rei da Prússia, em Potsdam, mesmo à saída de Berlim. É frequentemente incluído na lista dos palácios alemães rivais do Château de Versailles. Embora Sanssouci ostente o mais íntimo estilo Rococó e seja muito menor que o seu oponente construído no estilo Barroco Francês, é notável pelos numerosos templos e outras construções de jardim do seu Parque.',
          'audio/gb-sanssouci.wav'),
	
]

