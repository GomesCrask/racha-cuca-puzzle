// configurações da aplicação

const dificult = 1
const minGridSize = 2
const dificultMultiplyer = 1.3
let currentStage = 0
const levels = [
     new Level('images/br-cristo.jpeg',
 'Cristo Redentor é uma estátua art déco que retrata Jesus Cristo. Em 2007 foi eleito informalmente como uma das sete maravilhas do mundo moderno. Em 2012 a UNESCO considerou o Cristo Redentor como parte da paisagem do Rio de Janeiro incluída na lista de Patrimônios da Humanidade. Símbolo do cristianismo brasileiro, a estátua se tornou um ícone do Rio de Janeiro e do Brasil.',
 'audio/br-cristo.wav'),
 new Level('images/br-museu-das-artes-sao-paulo.jpg',
 'Museu de Arte de São Paulo Assis Chateaubriand é uma das mais importantes instituições culturais brasileiras. Instituição particular sem fins lucrativos, o museu foi fundado em 1947, por iniciativa do paraibano Assis Chateaubriand. Ao longo de sua história, notabilizou-se por uma série de iniciativas importantes no campo da museologia e da formação artística, bem como por sua forte atuação didática. Foi também um dos primeiros espaços museológicos do continente a atuar com perfil de centro cultural, bem como o primeiro museu do país a acolher as tendências artísticas surgidas após a Segunda Guerra Mundial.',
 'audio/br-museu-de-arte-de-sao-paulo.wav'), 
 new Level('images/br-museu-imperial.jpg',
 'O Museu Imperial, popularmente conhecido como Palácio Imperial, é um museu histórico-temático localizado no centro histórico da cidade de Petrópolis, no estado do Rio de Janeiro, no Brasil. O acervo do museu é constituído por peças ligadas à monarquia brasileira, incluindo mobiliário, documentos, obras de arte e objetos pessoais de integrantes da família imperial.',
 'audio/br-museu-imperial.wav'), 
 new Level('images/br-paraquedas-aves.jpg',
 'O Parque das Aves é um parque temático localizado na cidade de Foz do Iguaçu, no estado brasileiro do Paraná. Situado próximo às Cataratas do rio Iguaçu, o parque possui 16 hectares de mata nativa, com 1.500 animais entre aves, répteis e mamíferos, de 140 espécies diferentes. É uma instituição privada que trabalha como um centro de conservação integrada de espécies da Mata Atlântica, pesquisando a reprodução em cativeiro e preservando animais ameaçados de extinção.',
 'audio/br-parque-das-aves.wav'), 
 new Level('images/br-theatro-municipal-do-rio-de-janeiro.jpg',
 'O Theatro Municipal do Rio de Janeiro é um dos mais importantes teatros brasileiros. Localiza-se na Cinelândia, no centro da cidade do Rio de Janeiro. Apesar do nome, o teatro não pertence ao município, mas está vinculado ao estado do Rio de Janeiro. Atualmente o teatro é dirigido pela Fundação Theatro Municipal do Rio de Janeiro, que tem Aldo Mussi como presidente e André Heller-Lopes como diretor artístico.',
 'audio/br-teatro-municipal-do-rio-de-janeiro.wav'), 
] 

